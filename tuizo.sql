/*
SQLyog Ultimate v8.55 
MySQL - 5.5.8 : Database - tuizo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tuizo` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `tuizo`;

/*Table structure for table `admins` */

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `admin_id` int(5) NOT NULL AUTO_INCREMENT,
  `admin_full_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `admin_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `admin_password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `admin_hash` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `admin_type` enum('master','super','normal') COLLATE utf8_unicode_ci NOT NULL,
  `admin_status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL,
  `admin_last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `admin_update` datetime NOT NULL,
  `admin_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `admins` */

insert  into `admins`(`admin_id`,`admin_full_name`,`admin_email`,`admin_password`,`admin_hash`,`admin_type`,`admin_status`,`admin_last_login`,`admin_update`,`admin_updated_by`) values (1,'Faruk','faruk@bscheme.com','e10adc3949ba59abb#t1u1i1z1o1*e56e057f20f883e','c8tqs6lbgo4gfgul1bqsaj2054','super','active','2013-07-09 14:52:14','2013-04-27 16:10:46',0);

/*Table structure for table `cart_package_products` */

DROP TABLE IF EXISTS `cart_package_products`;

CREATE TABLE `cart_package_products` (
  `CPP_id` int(5) NOT NULL AUTO_INCREMENT,
  `CPP_session_id` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `CPP_package_id` int(5) NOT NULL,
  `CPP_product_category_id` int(5) NOT NULL,
  `CPP_product_id` int(5) NOT NULL,
  `CPP_quantity` int(2) NOT NULL,
  `CPP_date` datetime NOT NULL,
  PRIMARY KEY (`CPP_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='CPP= cart_package_products';

/*Data for the table `cart_package_products` */

/*Table structure for table `cart_packages` */

DROP TABLE IF EXISTS `cart_packages`;

CREATE TABLE `cart_packages` (
  `CP_id` int(5) NOT NULL AUTO_INCREMENT,
  `CP_user_id` int(11) NOT NULL COMMENT 'optional, if',
  `CP_session_id` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `CP_package_id` int(5) NOT NULL,
  `CP_price` decimal(2,0) NOT NULL,
  `CP_discount` decimal(2,0) NOT NULL,
  `CP_total_price` decimal(2,0) NOT NULL,
  `CP_date` datetime NOT NULL,
  PRIMARY KEY (`CP_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='CP= cart_packages';

/*Data for the table `cart_packages` */

/*Table structure for table `cash_on_delivery` */

DROP TABLE IF EXISTS `cash_on_delivery`;

CREATE TABLE `cash_on_delivery` (
  `COD_id` int(5) NOT NULL AUTO_INCREMENT,
  `COD_order_id` int(5) NOT NULL,
  `COD_user_id` int(11) NOT NULL,
  `COD_total_amount` decimal(2,0) NOT NULL,
  `COD_status` enum('unpaid','paid','closed') COLLATE utf8_unicode_ci NOT NULL COMMENT 'It will close when order closes',
  PRIMARY KEY (`COD_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='COD= cash_on_delivery';

/*Data for the table `cash_on_delivery` */

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `category_id` int(5) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `category_description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `category_parent_id` int(5) NOT NULL,
  `category_priority` int(11) NOT NULL,
  `category_logo` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `category_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category_updated_by` int(5) NOT NULL,
  `category_type` enum('user_created','builtin') COLLATE utf8_unicode_ci DEFAULT 'user_created',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `categories` */

insert  into `categories`(`category_id`,`category_name`,`category_description`,`category_parent_id`,`category_priority`,`category_logo`,`category_updated`,`category_updated_by`,`category_type`) values (1,'Brands','this is Brands',0,1,'Brands-1373182145.','2013-07-07 13:29:05',1,'builtin'),(2,'Categories','this is Brands',0,1,'Categories-1373182178.','2013-07-07 13:29:38',1,'builtin'),(3,'Basic','this is Brands',2,1,'Basic-1373182259.','2013-07-07 13:30:59',1,'builtin'),(4,'Premium','this is Brands',2,1,'Premium-1373182272.','2013-07-07 13:31:12',1,'builtin'),(5,'Tshirt','Tshirt',3,6,'Tshirt-1373352426.','2013-07-09 12:47:06',1,'user_created'),(6,'Tshirt','Tshirt',4,6,'Tshirt-1373352458.','2013-07-09 12:47:38',1,'user_created'),(7,'Shirt','Shirt',3,6,'Shirt-1373352484.','2013-07-09 12:48:04',1,'user_created'),(8,'Shirt','Shirt',4,6,'Shirt-1373352492.','2013-07-09 12:48:12',1,'user_created'),(9,'Panjabi','Panjabi',3,6,'Panjabi-1373352536.','2013-07-09 12:48:56',1,'user_created'),(10,'Panjabi','Panjabi',4,6,'Panjabi-1373352564.','2013-07-09 12:49:24',1,'user_created'),(11,'Truzer','Truzer',4,6,'Truzer-1373352586.','2013-07-09 12:49:46',1,'user_created'),(12,'Lungi','Lungi',3,6,'Lungi-1373352604.','2013-07-09 12:50:04',1,'user_created'),(13,'Tupi','Tupi',3,6,'Tupi-1373352652.','2013-07-09 12:50:52',1,'user_created'),(14,'Cap','Cap',4,6,'Cap-1373352667.','2013-07-09 12:51:07',1,'user_created');

/*Table structure for table `category_banners` */

DROP TABLE IF EXISTS `category_banners`;

CREATE TABLE `category_banners` (
  `CB_id` int(11) NOT NULL AUTO_INCREMENT,
  `CB_category_id` int(11) NOT NULL,
  `CB_image_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `CB_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `CB_priority` int(11) NOT NULL,
  `CB_description` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `CB_url` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `CB_url_type` enum('internal','external') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'internal' COMMENT 'if external set target="_blanck" on anchor tag',
  `CB_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CB_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`CB_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='category_banners = CB';

/*Data for the table `category_banners` */

insert  into `category_banners`(`CB_id`,`CB_category_id`,`CB_image_name`,`CB_title`,`CB_priority`,`CB_description`,`CB_url`,`CB_url_type`,`CB_updated`,`CB_updated_by`) values (8,5,'Category-0_Time-1370254593.png','Test category banner 1',0,'Test category banner 1','http://www.car.com','external','2013-06-03 16:16:33',1),(9,0,'Category-0_Time-1370323428.png','Product 1',1,'Product 1','http://Product 1','internal','2013-06-04 11:23:48',1),(10,2,'Category-2_Time-1370333311.png','Product 1',2,'Product 1','http://Product 1','internal','2013-06-04 14:08:31',1);

/*Table structure for table `cities` */

DROP TABLE IF EXISTS `cities`;

CREATE TABLE `cities` (
  `city_id` int(5) NOT NULL AUTO_INCREMENT,
  `city_country_id` int(5) NOT NULL,
  `city_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city_status` enum('allow','notallow') COLLATE utf8_unicode_ci NOT NULL,
  `city_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `city_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `cities` */

insert  into `cities`(`city_id`,`city_country_id`,`city_name`,`city_status`,`city_updated`,`city_updated_by`) values (1,3,'','notallow','0000-00-00 00:00:00',0),(2,3,'','notallow','0000-00-00 00:00:00',0),(3,2,'','notallow','0000-00-00 00:00:00',0),(4,2,'Dhaka','allow','0000-00-00 00:00:00',1),(5,1,'','notallow','0000-00-00 00:00:00',0),(6,1,'','notallow','0000-00-00 00:00:00',0),(7,1,'','notallow','0000-00-00 00:00:00',0),(8,4,'','notallow','0000-00-00 00:00:00',0),(9,4,'Rangamati','allow','0000-00-00 00:00:00',1),(10,3,'Dhaka','allow','0000-00-00 00:00:00',1),(11,4,' Comilla','notallow','0000-00-00 00:00:00',0),(12,3,'Dhaka','notallow','0000-00-00 00:00:00',0),(13,3,' Chittagong','notallow','0000-00-00 00:00:00',0),(14,3,' Rajshahi','notallow','0000-00-00 00:00:00',0),(15,3,'Dhaka','notallow','0000-00-00 00:00:00',0),(16,3,' Chittagong','notallow','0000-00-00 00:00:00',0),(17,3,' Rajshahi','notallow','0000-00-00 00:00:00',0),(18,3,'Dhaka','notallow','0000-00-00 00:00:00',0),(19,3,' Chittagong','notallow','0000-00-00 00:00:00',0),(20,3,' Rajshahi','notallow','0000-00-00 00:00:00',0),(21,3,'Dhaka','notallow','2013-06-20 13:08:02',1),(22,3,' Chittagong','notallow','2013-06-20 13:08:02',1),(23,3,' Rajshahi','notallow','2013-06-20 13:08:02',1),(24,3,'Dhaka','notallow','2013-06-20 13:13:47',1),(25,3,' Barisal','notallow','2013-06-20 13:13:47',1),(26,3,' Hghgh','notallow','2013-06-20 13:19:35',1),(27,3,' Narayangonj','notallow','2013-06-20 13:30:21',1),(28,2,'kkk','notallow','2013-06-20 15:43:39',1);

/*Table structure for table `colors` */

DROP TABLE IF EXISTS `colors`;

CREATE TABLE `colors` (
  `color_id` int(5) NOT NULL AUTO_INCREMENT,
  `color_title` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `color_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `color_image_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `color_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `color_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`color_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `colors` */

insert  into `colors`(`color_id`,`color_title`,`color_code`,`color_image_name`,`color_updated`,`color_updated_by`) values (1,'Test Color 1','470047','img1.jpg','2013-06-03 11:21:08',1),(2,'Test Color 2','7bff00','b6.jpg','2013-06-03 11:35:11',1),(3,'Product 1','ff00ff','b6.jpg','2013-06-03 15:21:41',1),(4,'AAaa 123 !!!!!!','ff00ff','img1.jpg','2013-06-03 17:28:06',1);

/*Table structure for table `config_settings` */

DROP TABLE IF EXISTS `config_settings`;

CREATE TABLE `config_settings` (
  `CS_option` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `CS_value` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='config_setting =CS';

/*Data for the table `config_settings` */

insert  into `config_settings`(`CS_option`,`CS_value`) values ('SITE_NAME','Ecommerce Framework'),('SITE_URL','http://localhost/ecommerce'),('SITE_LOGO','logo.jpg'),('SITE_FAVICON','favicon.ico'),('CATEGORY_BANNER_MAX_SIZE','10000'),('CATEGORY_BANNER_MAX_WIDTH','500'),('SITE_DEFAULT_META_TITLE','ecommerce'),('SITE_DEFAULT_META_DESCRIPTION','this default description'),('SITE_DEFAULT_META_KEYWORDS','cloth, bangladesh, dhaka, fashion'),('PRODUCT_LARGE_IMAGE_WIDTH','600'),('PRODUCT_MEDIUM_IMAGE_WIDTH','400'),('PRODUCT_SMALL_IMAGE_WIDTH','200'),('GOOGLE_ANALYTICS','place google analytics code here');

/*Table structure for table `countries` */

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `country_id` int(5) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `country_status` enum('allow','notallow') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'notallow',
  `country_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `country_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `countries` */

insert  into `countries`(`country_id`,`country_name`,`country_status`,`country_updated`,`country_updated_by`) values (1,'China','allow','2013-06-19 18:03:11',1),(2,'Australia','notallow','2013-06-19 18:03:21',1),(3,'Bangladesh','allow','2013-06-19 18:03:29',1),(4,'Russia','allow','2013-06-20 10:52:46',1);

/*Table structure for table `order_log` */

DROP TABLE IF EXISTS `order_log`;

CREATE TABLE `order_log` (
  `OL_id` int(5) NOT NULL AUTO_INCREMENT,
  `OL_order_id` int(5) NOT NULL COMMENT 'orders order_id',
  `OL_sender_id` int(11) NOT NULL,
  `OL_reciever_id` int(11) NOT NULL,
  `OL_messege` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `OL_date` datetime NOT NULL,
  `OL_read` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL COMMENT 'updated by receiver',
  PRIMARY KEY (`OL_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='OL= order_log ';

/*Data for the table `order_log` */

/*Table structure for table `order_products` */

DROP TABLE IF EXISTS `order_products`;

CREATE TABLE `order_products` (
  `OP_id` int(5) NOT NULL AUTO_INCREMENT,
  `OP_order_id` int(5) NOT NULL,
  `OP_user_id` int(11) NOT NULL,
  `OP_product_id` int(5) NOT NULL,
  `OP_product_inventory_id` int(11) NOT NULL,
  `OP_color_id` int(5) NOT NULL,
  `OP_size_id` int(5) NOT NULL,
  `OP_price` decimal(2,0) NOT NULL,
  `OP_discount` decimal(2,0) NOT NULL,
  `OP_product_quantity` int(5) NOT NULL,
  `OP_product_total_price` decimal(2,0) NOT NULL COMMENT 'total price = quantity * price',
  PRIMARY KEY (`OP_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='OP= order_products';

/*Data for the table `order_products` */

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_user_id` int(11) NOT NULL,
  `order_created` datetime NOT NULL,
  `order_number` varchar(150) COLLATE utf8_unicode_ci NOT NULL COMMENT 'default null. entered by admin when processing orders',
  `order_read` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL COMMENT 'if order has been viewed',
  `order_status` enum('booking','approved','delivered','paid','closed') COLLATE utf8_unicode_ci NOT NULL COMMENT 'Only can change at booking stage',
  `order_payment_type` enum('COD','Paypal','Walleto') COLLATE utf8_unicode_ci NOT NULL COMMENT 'create table for every option',
  `order_total_item` int(3) NOT NULL,
  `order_total_amount` decimal(2,0) NOT NULL COMMENT 'summation of all product price',
  `order_vat_amount` decimal(2,0) NOT NULL,
  `order_discount_amount` decimal(2,0) NOT NULL COMMENT 'summation of all product discount',
  `order_session_id` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `order_note` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `order_billing_first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `order_billing_middle_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `order_billing_last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `order_billing_phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `order_billing_address` text COLLATE utf8_unicode_ci NOT NULL,
  `order_billing_country` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `order_billing_city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `order_billing_zip` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `order_shipping_first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `order_shipping_middle_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `order_shipping_last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `order_shipping_phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `order_shipping_address` text COLLATE utf8_unicode_ci NOT NULL,
  `order_shipping_country` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `order_shipping_city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `order_shipping_zip` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `order_updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `orders` */

/*Table structure for table `package_booking` */

DROP TABLE IF EXISTS `package_booking`;

CREATE TABLE `package_booking` (
  `PB_id` int(5) NOT NULL AUTO_INCREMENT,
  `PB_user_id` int(11) NOT NULL,
  `PB_package_id` int(5) NOT NULL,
  `PB_price` decimal(2,0) NOT NULL,
  `PB_discount` decimal(2,0) NOT NULL,
  `PB_date` datetime NOT NULL,
  `PB_payment_type` enum('COD','Paypal','Walleto') COLLATE utf8_unicode_ci NOT NULL COMMENT 'create table for every option',
  `PB_status` enum('draft','confirmed','paid','close') COLLATE utf8_unicode_ci NOT NULL COMMENT 'booking can be deleted or cancelled only when status == draft, staus will chage to close when all ordered product on this package is closed',
  `PB_updated` datetime NOT NULL,
  `PB_updatedby` int(11) NOT NULL,
  `PB_expiery` date NOT NULL COMMENT 'default: booking day + 30 days',
  PRIMARY KEY (`PB_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='PB= package_booking';

/*Data for the table `package_booking` */

/*Table structure for table `package_booking_log` */

DROP TABLE IF EXISTS `package_booking_log`;

CREATE TABLE `package_booking_log` (
  `PBL_id` int(5) NOT NULL AUTO_INCREMENT,
  `PBL_pb_id` int(5) NOT NULL COMMENT 'package booking PB_id',
  `PBL_sender_id` int(11) NOT NULL,
  `PBL_reciever_id` int(11) NOT NULL,
  `PBL_messege` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `PBL_date` datetime NOT NULL,
  `PBL_read` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL COMMENT 'updated by receiver',
  PRIMARY KEY (`PBL_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='not deleteabel or editable, PBL= package_booking_log';

/*Data for the table `package_booking_log` */

/*Table structure for table `package_categories` */

DROP TABLE IF EXISTS `package_categories`;

CREATE TABLE `package_categories` (
  `PC_id` int(5) NOT NULL AUTO_INCREMENT,
  `PC_package_id` int(5) NOT NULL,
  `PC_catagory_id` int(5) NOT NULL COMMENT 'third level catagory or immidiate child of basic/premium',
  `PC_catagory_quantity` int(3) NOT NULL COMMENT 'number of products in that category',
  `PC_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PC_updatedby` int(11) NOT NULL,
  PRIMARY KEY (`PC_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='PC= package_categories ';

/*Data for the table `package_categories` */

/*Table structure for table `package_category_products` */

DROP TABLE IF EXISTS `package_category_products`;

CREATE TABLE `package_category_products` (
  `PCP_id` int(5) NOT NULL AUTO_INCREMENT,
  `PCP_package_id` int(5) NOT NULL,
  `PCP_package_category_id` int(5) NOT NULL COMMENT 'third level catagory or immidiate child of basic/premium',
  `PCP_product_id` int(5) NOT NULL COMMENT 'comes from product table',
  `PCP_created` datetime NOT NULL,
  `PCP_createdby` int(11) NOT NULL,
  PRIMARY KEY (`PCP_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='PCP= package_category_products';

/*Data for the table `package_category_products` */

/*Table structure for table `packages` */

DROP TABLE IF EXISTS `packages`;

CREATE TABLE `packages` (
  `package_id` int(5) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `package_description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `package_price` decimal(2,0) NOT NULL,
  `package_tax_class_id` int(5) NOT NULL,
  `package_discount` decimal(2,0) NOT NULL,
  `package_catagory_id` int(5) NOT NULL COMMENT 'second level category ID, cannot be changed, cannot delete these categories (remove delete option for these categories)',
  `package_logo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `package_banner` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `package_order_by` int(3) NOT NULL,
  `package_status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL,
  `package_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `package_updated_by` int(11) NOT NULL,
  `package_expiery` datetime NOT NULL,
  `package_sold_amount` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='package not deleteable or editable, only can deactivate from fronend view, deact';

/*Data for the table `packages` */

insert  into `packages`(`package_id`,`package_name`,`package_description`,`package_price`,`package_tax_class_id`,`package_discount`,`package_catagory_id`,`package_logo`,`package_banner`,`package_order_by`,`package_status`,`package_updated`,`package_updated_by`,`package_expiery`,`package_sold_amount`) values (1,'Package1','this is package one','12',1,'23',0,'Package1-logo-1.gif','Package1-banner-1.gif',12,'active','2013-07-08 19:31:18',1,'2013-07-27 00:00:00',1),(2,'Package1','this is package o','12',2,'99',3,'Package1-logo-2.gif','Package1-banner-2.gif',12,'inactive','2013-07-09 11:59:21',1,'2013-07-27 00:00:00',0),(3,'Package1','ggghhh','1',1,'34',3,'Package1-logo-3.gif','Package1-banner-3.jpg',12,'inactive','2013-07-07 20:04:56',1,'2013-07-04 00:00:00',0),(4,'Package2','ggghhh','1',1,'34',3,'Package2-logo-4.gif','Package2-banner-4.gif',12,'active','2013-07-08 19:03:31',1,'2013-07-04 00:00:00',0),(5,'Package2','12','1',1,'45',4,'Package2-logo-5.gif','Package2-banner-5.jpg',12,'inactive','2013-07-08 19:03:11',1,'2013-07-13 00:00:00',0),(6,'Five','Five','12',1,'7',4,'Five-logo-6.gif','Five-banner-6.gif',12,'inactive','2013-07-07 20:10:47',1,'2013-07-10 00:00:00',0),(7,'Five','Five','12',1,'7',3,'Five-logo-7.gif','Five-banner-7.jpg',12,'inactive','2013-07-09 12:02:42',1,'2013-07-10 00:00:00',0),(8,'Five','Five','12',1,'7',3,'Five-logo-8.gif','Five-banner-8.jpg',12,'inactive','2013-07-07 20:11:24',1,'2013-07-10 00:00:00',0),(9,'Package1','12','12',1,'34',3,'Package1-logo-9.gif','Package1-banner-9.gif',12,'inactive','2013-07-08 15:28:45',1,'2013-07-25 00:00:00',0),(10,'Package3','12','12',1,'12',3,'','',12,'active','2013-07-08 15:30:14',1,'2013-07-29 00:00:00',0),(11,'package4','sdfasddfdfdf','99',1,'34',4,'package4-logo-11.gif','package4-banner-11.gif',1,'active','2013-07-08 18:42:55',1,'0000-00-00 00:00:00',0);

/*Table structure for table `paypal_payment` */

DROP TABLE IF EXISTS `paypal_payment`;

CREATE TABLE `paypal_payment` (
  `PP_id` int(5) NOT NULL AUTO_INCREMENT,
  `PP_order_id` int(5) NOT NULL,
  `PP_user_id` int(11) NOT NULL,
  `PP_total_amount` decimal(2,0) NOT NULL,
  `PP_status` enum('unpaid','paid','closed') COLLATE utf8_unicode_ci NOT NULL COMMENT 'It will close when order closes',
  `PP_payment_gross` decimal(2,0) NOT NULL,
  `PP_payment_currency` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PP_varify_sign` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `PP_payer_email` int(150) NOT NULL,
  `PP_receiver_email` varchar(150) COLLATE utf8_unicode_ci NOT NULL COMMENT 'site owner or shop owner',
  `PP_txn_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `PP_ip_track_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `PP_full_data` text COLLATE utf8_unicode_ci NOT NULL,
  `PP_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PP_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='PP= paypal_payment';

/*Data for the table `paypal_payment` */

/*Table structure for table `product_also_like` */

DROP TABLE IF EXISTS `product_also_like`;

CREATE TABLE `product_also_like` (
  `PAL_id` int(11) NOT NULL AUTO_INCREMENT,
  `PAL_current_product_id` int(11) NOT NULL,
  `PAL_related_product_id` int(11) NOT NULL COMMENT '(not equal to current product ID, if current product ID =4 , related product ID cannot be 4)',
  `PAL_priority_id` int(3) NOT NULL,
  `PAL_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PAL_created_by_id` int(11) NOT NULL,
  PRIMARY KEY (`PAL_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='product_also_like = PAL';

/*Data for the table `product_also_like` */

insert  into `product_also_like`(`PAL_id`,`PAL_current_product_id`,`PAL_related_product_id`,`PAL_priority_id`,`PAL_created`,`PAL_created_by_id`) values (5,1,5,0,'2013-05-25 17:25:05',1);

/*Table structure for table `product_categories` */

DROP TABLE IF EXISTS `product_categories`;

CREATE TABLE `product_categories` (
  `PC_id` int(11) NOT NULL AUTO_INCREMENT,
  `PC_product_id` int(11) NOT NULL,
  `PC_category_id` int(11) NOT NULL,
  `PC_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PC_created_by` int(11) NOT NULL,
  PRIMARY KEY (`PC_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='product_categories = PC';

/*Data for the table `product_categories` */

insert  into `product_categories`(`PC_id`,`PC_product_id`,`PC_category_id`,`PC_created`,`PC_created_by`) values (2,2,0,'2013-05-18 16:28:05',0),(7,4,1,'2013-05-26 13:05:18',0),(8,11,0,'2013-05-30 16:52:55',0),(13,1,0,'2013-06-02 11:22:22',0);

/*Table structure for table `product_discounts` */

DROP TABLE IF EXISTS `product_discounts`;

CREATE TABLE `product_discounts` (
  `PD_id` int(11) NOT NULL AUTO_INCREMENT,
  `PD_product_id` int(11) NOT NULL,
  `PD_start_date` date NOT NULL,
  `PD_end_date` date NOT NULL,
  `PD_amount` float NOT NULL,
  `PD_status` int(11) NOT NULL DEFAULT '1',
  `PD_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PD_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`PD_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `product_discounts` */

insert  into `product_discounts`(`PD_id`,`PD_product_id`,`PD_start_date`,`PD_end_date`,`PD_amount`,`PD_status`,`PD_updated`,`PD_updated_by`) values (1,5,'2013-06-01','2013-06-05',5,0,'0000-00-00 00:00:00',0),(2,5,'2013-06-08','2013-06-10',5,0,'0000-00-00 00:00:00',0),(3,5,'2013-06-11','2013-06-15',10,0,'2013-06-11 16:26:54',1),(4,4,'2013-06-01','2013-06-10',15,0,'2013-06-15 14:38:44',1),(5,0,'2013-06-01','2013-06-10',5,1,'2013-06-15 15:20:09',1),(7,1,'2013-06-25','2013-06-26',9,0,'2013-06-15 15:45:56',1),(8,4,'2013-06-01','2013-06-10',9,1,'2013-06-16 10:35:38',1),(9,4,'2013-06-11','2013-06-12',10,0,'2013-06-16 10:47:09',1),(10,4,'2013-06-11','2013-06-12',15,0,'2013-06-16 10:48:33',1),(11,4,'2013-06-11','2013-06-12',15,0,'2013-06-16 10:50:01',1),(12,4,'2013-06-15','2013-06-16',15,0,'2013-06-16 10:50:09',1),(13,1,'2013-06-01','2013-06-04',5,0,'2013-06-18 14:14:40',1),(14,1,'2013-06-05','2013-06-10',5,0,'2013-06-18 14:14:54',1),(15,1,'2013-06-05','2013-06-10',5,0,'2013-06-18 14:15:25',1);

/*Table structure for table `product_images` */

DROP TABLE IF EXISTS `product_images`;

CREATE TABLE `product_images` (
  `PI_id` int(11) NOT NULL AUTO_INCREMENT,
  `PI_product_id` int(11) NOT NULL,
  `PI_file_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `PI_priority` int(6) NOT NULL,
  `PI_color` int(11) NOT NULL,
  `PI_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PI_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`PI_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='PI = product_images';

/*Data for the table `product_images` */

insert  into `product_images`(`PI_id`,`PI_product_id`,`PI_file_name`,`PI_priority`,`PI_color`,`PI_updated`,`PI_updated_by`) values (17,3,'3-1368869607.jpg',0,1,'2013-05-18 15:33:27',1),(20,3,'3-1368869899.jpg',0,1,'2013-05-18 15:38:19',1),(21,1,'1-1368871859.jpg',0,1,'2013-05-18 16:10:59',1),(22,2,'2-1368872859.jpg',0,1,'2013-05-18 16:27:39',1),(23,0,'-1369126927.ico',0,1,'2013-05-21 15:02:07',1),(24,0,'-1369127171.jpg',0,1,'2013-05-21 15:06:11',1),(27,5,'5-1369131076.jpg',0,1,'2013-05-21 16:11:16',1),(28,5,'5-1369131151.jpg',0,1,'2013-05-21 16:12:31',1),(29,4,'4-1370238586.jpg',0,1,'2013-06-03 11:49:46',1),(31,1,'1-1373355424.gif',0,1,'2013-07-09 13:37:04',1),(32,1,'1-1373355540.gif',0,0,'2013-07-09 13:39:00',1);

/*Table structure for table `product_inventories` */

DROP TABLE IF EXISTS `product_inventories`;

CREATE TABLE `product_inventories` (
  `PI_id` int(11) NOT NULL AUTO_INCREMENT,
  `PI_product_id` int(11) NOT NULL,
  `PI_color_id` int(11) NOT NULL,
  `PI_size_id` int(11) NOT NULL,
  `PI_quantity` int(5) NOT NULL,
  `PI_weight` int(5) NOT NULL,
  `PI_cost` decimal(10,0) NOT NULL,
  `PI_price` decimal(10,0) NOT NULL,
  `PI_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PI_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`PI_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='PI = product_inventories';

/*Data for the table `product_inventories` */

insert  into `product_inventories`(`PI_id`,`PI_product_id`,`PI_color_id`,`PI_size_id`,`PI_quantity`,`PI_weight`,`PI_cost`,`PI_price`,`PI_updated`,`PI_updated_by`) values (1,1,1,1,10,100,'1000','10','2013-06-02 11:25:09',1),(2,1,1,2,2,234,'12','11','2013-07-09 13:43:38',1),(3,1,2,4,2,234,'12','11','2013-07-09 13:43:57',1),(4,1,1,2,2,234,'12','0','2013-07-09 14:58:26',1),(5,1,1,2,2,234,'12','0','2013-07-09 15:16:37',1);

/*Table structure for table `product_rating` */

DROP TABLE IF EXISTS `product_rating`;

CREATE TABLE `product_rating` (
  `PR_id` int(5) NOT NULL AUTO_INCREMENT,
  `PR_product_id` int(5) NOT NULL,
  `PR_user_id` int(11) NOT NULL,
  `PR_rating_amount` int(5) NOT NULL,
  PRIMARY KEY (`PR_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `product_rating` */

/*Table structure for table `product_review` */

DROP TABLE IF EXISTS `product_review`;

CREATE TABLE `product_review` (
  `PRE_id` int(11) NOT NULL AUTO_INCREMENT,
  `PRE_product_id` int(11) NOT NULL,
  `PRE_user_id` int(5) NOT NULL,
  `PRE_comment` varchar(500) NOT NULL,
  `PRE_status` enum('draft','publish') NOT NULL,
  `PRE_read` enum('yes','no') NOT NULL,
  PRIMARY KEY (`PRE_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `product_review` */

insert  into `product_review`(`PRE_id`,`PRE_product_id`,`PRE_user_id`,`PRE_comment`,`PRE_status`,`PRE_read`) values (1,3,1,'This is a sample review This is a sample review This is a sample review This is a sample review','draft','no'),(2,1,1,'This is a product review and you all can view it and read it easily.','draft',''),(3,1,1,'This is a product review and you all can view it and read it easily.','draft','yes');

/*Table structure for table `product_tags` */

DROP TABLE IF EXISTS `product_tags`;

CREATE TABLE `product_tags` (
  `PT_id` int(11) NOT NULL AUTO_INCREMENT,
  `PT_product_id` int(11) NOT NULL,
  `PT_tag_id` int(11) NOT NULL,
  `PT_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PT_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`PT_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='PT = product_tags';

/*Data for the table `product_tags` */

insert  into `product_tags`(`PT_id`,`PT_product_id`,`PT_tag_id`,`PT_updated`,`PT_updated_by`) values (1,1,1,'2013-05-25 17:46:06',1),(2,1,3,'2013-05-25 17:46:06',1),(3,1,6,'2013-05-25 17:46:06',1),(4,1,7,'2013-05-25 17:46:37',1),(5,1,8,'2013-05-25 17:46:37',1),(6,11,2,'2013-05-30 17:17:39',1),(7,11,3,'2013-05-30 17:17:39',1),(8,1,3,'2013-06-02 11:27:04',1),(9,1,9,'2013-06-02 11:27:15',1);

/*Table structure for table `product_view` */

DROP TABLE IF EXISTS `product_view`;

CREATE TABLE `product_view` (
  `PV_id` int(15) NOT NULL AUTO_INCREMENT,
  `PV_product_id` int(5) NOT NULL,
  `PV_session_id` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `PV_visitor_ip` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PV_created` datetime NOT NULL,
  PRIMARY KEY (`PV_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='empty regularly using cron, PV= product_view table';

/*Data for the table `product_view` */

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `product_sku` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `product_short_description` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `product_long_description` text COLLATE utf8_unicode_ci NOT NULL,
  `product_meta_title` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `product_meta_keywords` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `product_meta_description` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `product_quantity` int(4) NOT NULL,
  `product_weight` int(5) NOT NULL,
  `product_cost` decimal(10,0) NOT NULL,
  `product_price` decimal(10,0) NOT NULL,
  `product_discount_price` decimal(10,0) NOT NULL,
  `product_show_as_new_from` date NOT NULL,
  `product_show_as_new_to` date NOT NULL,
  `product_show_as_featured_from` date NOT NULL,
  `product_show_as_featured_to` date NOT NULL,
  `product_status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL,
  `product_total_viewed` int(11) NOT NULL,
  `product_tatal_sale` int(11) NOT NULL,
  `product_tax_class_id` int(11) NOT NULL,
  `product_avg_rating` int(2) NOT NULL,
  `product_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `product_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='weight will be gram';

/*Data for the table `products` */

insert  into `products`(`product_id`,`product_title`,`product_sku`,`product_short_description`,`product_long_description`,`product_meta_title`,`product_meta_keywords`,`product_meta_description`,`product_quantity`,`product_weight`,`product_cost`,`product_price`,`product_discount_price`,`product_show_as_new_from`,`product_show_as_new_to`,`product_show_as_featured_from`,`product_show_as_featured_to`,`product_status`,`product_total_viewed`,`product_tatal_sale`,`product_tax_class_id`,`product_avg_rating`,`product_updated`,`product_updated_by`) values (1,'One ','1122','my short description','my long description ','asdad111','asdads23123','asdasda1111',0,0,'1','112','0','0000-00-00','0000-00-00','0000-00-00','0000-00-00','active',0,0,0,4,'2013-05-14 10:14:59',1),(2,'two','125','short description','description dwo ','asdad111','asdads23123','asdasda1111',0,0,'1','112','0','0000-00-00','0000-00-00','0000-00-00','0000-00-00','active',0,0,0,0,'2013-05-14 10:17:45',1),(3,'rwerwe','rwerwe','qweqwe','rewrew','asdad111','asdads23123','asdasda1111',0,0,'1','112','0','0000-00-00','0000-00-00','0000-00-00','0000-00-00','active',0,0,0,0,'2013-05-18 11:16:01',1),(4,'ewr','wer','','wer','asdad111','asdads23123','asdasda1111',0,0,'1','112','0','0000-00-00','0000-00-00','0000-00-00','0000-00-00','active',0,0,0,0,'2013-05-18 12:18:02',1),(5,'asdasda','sdasdasd','','asdasddasda','asdad111','asdads23123','asdasda1111',0,0,'1','112','0','0000-00-00','0000-00-00','0000-00-00','0000-00-00','active',0,0,0,0,'2013-05-20 16:19:31',1),(6,'qwqw','qwqw','','qwqw','asdad111','asdads23123','asdasda1111',0,0,'1','112','0','0000-00-00','0000-00-00','0000-00-00','0000-00-00','active',0,0,0,0,'2013-05-20 16:26:05',1),(7,'qwqw1','wewe1','','wewe2','asdad111','asdads23123','asdasda1111',0,0,'1','112','0','0000-00-00','0000-00-00','0000-00-00','0000-00-00','active',0,0,0,0,'2013-05-21 12:54:11',1),(8,'dsdfsdf','asdad123!!','','sdfsf','asdad111','asdads23123','asdasda1111',0,0,'1','112','0','0000-00-00','0000-00-00','0000-00-00','0000-00-00','active',0,0,0,0,'2013-05-21 14:31:16',1),(9,'asdasda','ABCE123456','','qweqwe','asdad111','asdads23123','asdasda1111',0,0,'1','112','0','0000-00-00','0000-00-00','0000-00-00','0000-00-00','active',0,0,0,0,'2013-05-21 14:50:38',1),(10,'asda','ABCD123456','','asdasd','asdad111','asdads23123','asdasda1111',0,0,'1','112','0','0000-00-00','0000-00-00','0000-00-00','0000-00-00','active',0,0,0,0,'2013-05-22 12:51:06',1),(11,'Test Product 11','ABCD123456','Short description','Test Product 11','asdad111','asdads23123','asdasda1111',0,0,'1','112','0','0000-00-00','0000-00-00','0000-00-00','0000-00-00','active',0,0,0,0,'2013-05-30 14:09:48',1),(12,'sasasfassfasfasfasfas','fasfasfasfasfas','','asfasfasfasfasfaf','','','',0,0,'1','112','0','0000-00-00','0000-00-00','0000-00-00','0000-00-00','active',0,0,0,0,'2013-06-11 13:25:35',1),(13,'sadasdasd','asdasdasd','','asdasdasdasd','','','',0,0,'1','112','0','0000-00-00','0000-00-00','0000-00-00','0000-00-00','active',0,0,0,0,'2013-06-11 13:26:09',1),(14,'asdasdasdasd','asd12','','asdasdasdasdasdad','','','',0,0,'1','112','0','0000-00-00','0000-00-00','0000-00-00','0000-00-00','active',0,0,0,7,'2013-06-11 13:27:21',1),(15,'sasdasdasdasdadasd','sasdasdasdasdadasd','','sasdasdasdasdadasd','','','',0,0,'1','112','0','0000-00-00','0000-00-00','0000-00-00','0000-00-00','active',0,0,0,5,'2013-06-11 13:29:29',1);

/*Table structure for table `products_related` */

DROP TABLE IF EXISTS `products_related`;

CREATE TABLE `products_related` (
  `PR_id` int(11) NOT NULL AUTO_INCREMENT,
  `PR_current_product_id` int(11) NOT NULL,
  `PR_related_product_id` int(11) NOT NULL COMMENT '(not equal to current product ID, if current product ID =4 , related product ID cannot be 4)',
  `PR_priority_id` int(3) NOT NULL,
  `PR_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PR_created_by_id` int(11) NOT NULL,
  PRIMARY KEY (`PR_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='products_related = PR';

/*Data for the table `products_related` */

insert  into `products_related`(`PR_id`,`PR_current_product_id`,`PR_related_product_id`,`PR_priority_id`,`PR_created`,`PR_created_by_id`) values (1,1,2,0,'2013-05-25 15:13:48',1);

/*Table structure for table `products_upsell` */

DROP TABLE IF EXISTS `products_upsell`;

CREATE TABLE `products_upsell` (
  `PU_id` int(11) NOT NULL AUTO_INCREMENT,
  `PU_current_product_id` int(11) NOT NULL,
  `PU_related_product_id` int(11) NOT NULL COMMENT '(not equal to current product ID, if current product ID =4 , related product ID cannot be 4)',
  `PU_priority_id` int(3) NOT NULL,
  `PU_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PU_created_by_id` int(11) NOT NULL,
  PRIMARY KEY (`PU_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='products_upsell = PU';

/*Data for the table `products_upsell` */

insert  into `products_upsell`(`PU_id`,`PU_current_product_id`,`PU_related_product_id`,`PU_priority_id`,`PU_created`,`PU_created_by_id`) values (1,1,2,0,'2013-05-25 17:01:19',1),(2,1,10,5,'2013-05-25 17:01:54',1),(3,6,2,0,'2013-07-09 12:39:59',1),(6,6,10,5,'2013-07-09 12:39:59',1);

/*Table structure for table `sizes` */

DROP TABLE IF EXISTS `sizes`;

CREATE TABLE `sizes` (
  `size_id` int(5) NOT NULL AUTO_INCREMENT,
  `size_title` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `size_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `size_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`size_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `sizes` */

insert  into `sizes`(`size_id`,`size_title`,`size_updated`,`size_updated_by`) values (1,'30-45','2013-04-29 14:36:17',1),(2,'40-41','2013-04-29 14:37:26',1),(4,'20-21','2013-04-29 14:45:39',1),(5,'20-25','2013-04-30 12:52:00',1),(6,'40-41','2013-06-03 14:17:59',1);

/*Table structure for table `tags` */

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `tag_id` int(5) NOT NULL AUTO_INCREMENT,
  `tag_title` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tag_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tag_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tags` */

insert  into `tags`(`tag_id`,`tag_title`,`tag_updated`,`tag_updated_by`) values (1,'asdad2232342342','2013-04-28 11:07:43',1),(2,'asdasdasd','2013-04-29 11:43:03',1),(3,'asdasdggrrgrrrgrg','2013-04-29 11:49:05',1),(4,'asda,asdasasd,asdad','2013-04-29 12:10:47',1),(6,'asdasd','2013-04-29 12:32:53',1),(7,'dasdasda','2013-04-30 12:42:46',1),(8,'asdasdasdasdasdasd','2013-04-30 12:48:17',1),(9,'asdasdad','2013-05-20 16:18:57',1);

/*Table structure for table `tax_classes` */

DROP TABLE IF EXISTS `tax_classes`;

CREATE TABLE `tax_classes` (
  `TC_id` int(5) NOT NULL AUTO_INCREMENT,
  `TC_title` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `TC_percent` decimal(10,0) NOT NULL,
  `TC_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `TC_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`TC_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='TC = tax_classes';

/*Data for the table `tax_classes` */

insert  into `tax_classes`(`TC_id`,`TC_title`,`TC_percent`,`TC_updated`,`TC_updated_by`) values (1,'asda','19','2013-04-30 14:31:43',1),(2,'ewr','10','2013-04-30 14:37:00',1);

/*Table structure for table `temp_carts` */

DROP TABLE IF EXISTS `temp_carts`;

CREATE TABLE `temp_carts` (
  `TC_id` int(5) NOT NULL AUTO_INCREMENT,
  `TC_session_id` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TC_product_id` int(5) NOT NULL,
  `TC_product_inventory_id` int(5) NOT NULL,
  `TC_color_id` int(5) NOT NULL,
  `TC_size_id` int(5) NOT NULL,
  `TC_price` decimal(2,0) NOT NULL,
  `TC_discount_amount` decimal(2,0) NOT NULL,
  `TC_product_quantity` int(5) NOT NULL,
  `TC_product_total_price` decimal(2,0) NOT NULL COMMENT 'total price = quantity * price',
  `TC_created` datetime NOT NULL,
  PRIMARY KEY (`TC_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='empty regularly using cron, TC= temp_carts';

/*Data for the table `temp_carts` */

/*Table structure for table `user_addresses` */

DROP TABLE IF EXISTS `user_addresses`;

CREATE TABLE `user_addresses` (
  `UA_id` int(5) NOT NULL AUTO_INCREMENT,
  `UA_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `UA_first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `UA_middle_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `UA_last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `UA_phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `UA_best_call_time` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `UA_country_id` int(5) NOT NULL,
  `UA_city_id` int(5) NOT NULL,
  `UA_zip` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `UA_address` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `UA_updated` datetime NOT NULL,
  PRIMARY KEY (`UA_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='UA= user_addresses';

/*Data for the table `user_addresses` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_hash` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `user_status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL,
  `user_first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_middle_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_DOB` date NOT NULL COMMENT 'date of birth',
  `user_gender` enum('male','female') COLLATE utf8_unicode_ci NOT NULL,
  `user_aboutme` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `user_profile_image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_address` text COLLATE utf8_unicode_ci NOT NULL,
  `user_phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_last_login` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`user_id`,`user_email`,`user_password`,`user_hash`,`user_status`,`user_first_name`,`user_middle_name`,`user_last_name`,`user_DOB`,`user_gender`,`user_aboutme`,`user_profile_image`,`user_address`,`user_phone`,`user_last_login`) values (1,'mijo.jfwg@gmail.com','123456','','active','William','Francis','Gomes','1986-07-10','male','I am William','','Sample Address','01914272921','0000-00-00 00:00:00'),(2,'mijo.jfwg@gmail.com','123456','','active','William','Francis','Gomes','1986-07-10','male','I am William','','Sample Address','01914272921','1986-07-10 00:00:00');

/*Table structure for table `walleto_payment` */

DROP TABLE IF EXISTS `walleto_payment`;

CREATE TABLE `walleto_payment` (
  `WP_id` int(5) NOT NULL AUTO_INCREMENT,
  `WP_order_id` int(5) NOT NULL,
  `WP_user_id` int(11) NOT NULL,
  `WP_total_amount` decimal(2,0) NOT NULL,
  `WP_status` enum('unpaid','paid','closed') COLLATE utf8_unicode_ci NOT NULL COMMENT 'It will close when order closes',
  PRIMARY KEY (`WP_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='design based on walleto api, WP= walleto_payment';

/*Data for the table `walleto_payment` */

/*Table structure for table `wishlist_package_products` */

DROP TABLE IF EXISTS `wishlist_package_products`;

CREATE TABLE `wishlist_package_products` (
  `WPP_id` int(5) NOT NULL AUTO_INCREMENT,
  `WPP_package_id` int(5) NOT NULL COMMENT 'check before adding both wishlist_packages and wishlist_products',
  `WPP_product_id` int(5) NOT NULL,
  `WPP_user_id` int(11) NOT NULL,
  `WPP_date` datetime NOT NULL,
  PRIMARY KEY (`WPP_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='WPP= wishlist_package_products';

/*Data for the table `wishlist_package_products` */

/*Table structure for table `wishlist_packages` */

DROP TABLE IF EXISTS `wishlist_packages`;

CREATE TABLE `wishlist_packages` (
  `WPA_id` int(5) NOT NULL AUTO_INCREMENT,
  `WPA_package_id` int(5) NOT NULL COMMENT 'check before adding both wishlist_packages and wishlist_products',
  `WPA_user_id` int(5) NOT NULL,
  `WPA_date` datetime NOT NULL,
  PRIMARY KEY (`WPA_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='WPA=wishlist_packages';

/*Data for the table `wishlist_packages` */

/*Table structure for table `wishlists` */

DROP TABLE IF EXISTS `wishlists`;

CREATE TABLE `wishlists` (
  `WL_id` int(5) NOT NULL AUTO_INCREMENT,
  `WL_product_id` int(5) NOT NULL,
  `WL_user_id` int(11) NOT NULL,
  `WL_cteated` datetime NOT NULL,
  PRIMARY KEY (`WL_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='WL= wishlists ';

/*Data for the table `wishlists` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
