-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 20, 2013 at 03:23 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `admin_id` int(5) NOT NULL AUTO_INCREMENT,
  `admin_full_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `admin_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `admin_password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `admin_hash` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `admin_type` enum('master','super','normal') COLLATE utf8_unicode_ci NOT NULL,
  `admin_status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL,
  `admin_last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `admin_update` datetime NOT NULL,
  `admin_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `admin_full_name`, `admin_email`, `admin_password`, `admin_hash`, `admin_type`, `admin_status`, `admin_last_login`, `admin_update`, `admin_updated_by`) VALUES
(1, 'Faruk', 'faruk@bscheme.com', 'e10adc3949ba59abb#e3c201m2r1*e56e057f20f883e', '8777vt19s12u4c2bf00d123nb3', 'super', 'active', '2013-06-20 12:16:53', '2013-04-27 16:10:46', 0),
(2, 'faruk 5', 'faruk+5@gmail.com', 'd58e3582afa99040e#e3c201m2r1*27b92b13c8f2280', '384664#e3c201m2r1*538299', 'normal', 'inactive', '2013-04-27 03:54:53', '2013-04-27 16:09:36', 0),
(3, 'faruk1', 'faruk+1@gmail.com', '4607e782c4d86fd53#e3c201m2r1*64d7e4508bb10d9', '867276#e3c201m2r1*801477', 'normal', 'inactive', '2013-04-27 04:01:47', '2013-04-27 16:01:47', 0),
(4, 'faruk2', 'faruk+2@gmail.com', '4607e782c4d86fd53#e3c201m2r1*64d7e4508bb10d9', '773195#e3c201m2r1*844488', 'normal', 'inactive', '2013-04-27 04:06:38', '2013-04-27 16:06:38', 0),
(5, 'sdasdasdasd', 'faruk@sample.com', 'e10adc3949ba59abb#e3c201m2r1*e56e057f20f883e', '97192#e3c201m2r1*669723', 'super', 'inactive', '2013-05-20 10:10:08', '2013-05-20 16:10:08', 0),
(6, 'mizan', 'mizan@bscheme.com', '827ccb0eea8a706c4#e3c201m2r1*c34a16891f84e7b', '927067#e3c201m2r1*383969', 'normal', 'inactive', '2013-06-02 06:29:48', '2013-06-02 12:29:48', 0),
(7, 'mizan', 'admin@admin.com', '827ccb0eea8a706c4#e3c201m2r1*c34a16891f84e7b', '105712#e3c201m2r1*473342', 'normal', 'inactive', '2013-06-02 06:31:37', '2013-06-15 13:02:05', 0),
(8, 'mizan', 'mizan1@bscheme.com', 'fcea920f7412b5da7#e3c201m2r1*be0cf42b8c93759', '9l9dufph9rb69114aq7gh5ufe6', 'normal', 'active', '2013-06-02 09:57:20', '2013-06-02 15:00:46', 0),
(9, 'sdasdasdasd', 'faruk@hello.com', 'e10adc3949ba59abb#e3c201m2r1*e56e057f20f883e', '587147#e3c201m2r1*830700', 'normal', 'inactive', '2013-06-03 11:07:05', '2013-06-03 17:07:05', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cash_on_delivery`
--

CREATE TABLE IF NOT EXISTS `cash_on_delivery` (
  `COD_id` int(5) NOT NULL AUTO_INCREMENT,
  `COD_order_id` int(5) NOT NULL,
  `COD_user_id` int(11) NOT NULL,
  `COD_total_amount` decimal(2,0) NOT NULL,
  `COD_status` enum('unpaid','paid','closed') COLLATE utf8_unicode_ci NOT NULL COMMENT 'It will close when order closes',
  PRIMARY KEY (`COD_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='COD= cash_on_delivery' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(5) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `category_description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `category_parent_id` int(5) NOT NULL,
  `category_priority` int(11) NOT NULL,
  `category_logo` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `category_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `category_description`, `category_parent_id`, `category_priority`, `category_logo`, `category_updated`, `category_updated_by`) VALUES
(1, 'cat123', 'cat1 desc23', 2, 0, '', '2013-05-21 11:04:20', 1),
(2, 'Product 1', 'Product 1', 0, 0, '', '2013-05-26 08:48:25', 1),
(3, 'car', 'car', 0, 0, '', '2013-05-26 08:48:39', 1),
(4, 'Toyota', 'Toyota', 2, 0, '', '2013-05-26 08:48:49', 1),
(5, 'Test Category 1', 'Test Category 1', 0, 0, '', '2013-06-03 06:03:54', 1),
(6, 'AAss 123', 'AAss 123', 0, 0, '', '2013-06-03 11:05:03', 1),
(7, 'AAss !!!123', 'AAss 123', 0, 0, '', '2013-06-03 11:05:14', 1),
(8, 'cat123', 'cat123', 2, 0, '', '2013-06-04 10:31:15', 1),
(9, 'Toyota', 'Toyota', 0, 12, 'Toyota-1370342367.', '2013-06-04 10:39:27', 1),
(10, 'Toyota', 'Toyota', 7, 1, 'Toyota-1370342493.', '2013-06-04 10:41:33', 1),
(11, 'Product 1', 'Product 1', 7, 11, 'Product 1-1370342530.jpg', '2013-06-04 10:42:10', 1),
(12, 'Product 1', 'Product 1', 9, 1, 'Product 1-1370343130.jpg', '2013-06-04 10:52:10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category_banners`
--

CREATE TABLE IF NOT EXISTS `category_banners` (
  `CB_id` int(11) NOT NULL AUTO_INCREMENT,
  `CB_category_id` int(11) NOT NULL,
  `CB_image_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `CB_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `CB_priority` int(11) NOT NULL,
  `CB_description` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `CB_url` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `CB_url_type` enum('internal','external') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'internal' COMMENT 'if external set target="_blanck" on anchor tag',
  `CB_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CB_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`CB_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='category_banners = CB' AUTO_INCREMENT=11 ;

--
-- Dumping data for table `category_banners`
--

INSERT INTO `category_banners` (`CB_id`, `CB_category_id`, `CB_image_name`, `CB_title`, `CB_priority`, `CB_description`, `CB_url`, `CB_url_type`, `CB_updated`, `CB_updated_by`) VALUES
(8, 5, 'Category-0_Time-1370254593.png', 'Test category banner 1', 0, 'Test category banner 1', 'http://www.car.com', 'external', '2013-06-03 10:16:33', 1),
(9, 0, 'Category-0_Time-1370323428.png', 'Product 1', 1, 'Product 1', 'http://Product 1', 'internal', '2013-06-04 05:23:48', 1),
(10, 2, 'Category-2_Time-1370333311.png', 'Product 1', 2, 'Product 1', 'http://Product 1', 'internal', '2013-06-04 08:08:31', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `city_id` int(5) NOT NULL AUTO_INCREMENT,
  `city_country_id` int(5) NOT NULL,
  `city_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city_status` enum('allow','notallow') COLLATE utf8_unicode_ci NOT NULL,
  `city_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `city_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`city_id`, `city_country_id`, `city_name`, `city_status`, `city_updated`, `city_updated_by`) VALUES
(1, 3, '', 'notallow', '0000-00-00 00:00:00', 0),
(2, 3, '', 'notallow', '0000-00-00 00:00:00', 0),
(3, 2, '', 'notallow', '0000-00-00 00:00:00', 0),
(4, 2, 'Dhaka', 'allow', '0000-00-00 00:00:00', 1),
(5, 1, '', 'notallow', '0000-00-00 00:00:00', 0),
(6, 1, '', 'notallow', '0000-00-00 00:00:00', 0),
(7, 1, '', 'notallow', '0000-00-00 00:00:00', 0),
(8, 4, '', 'notallow', '0000-00-00 00:00:00', 0),
(9, 4, 'Rangamati', 'allow', '0000-00-00 00:00:00', 1),
(10, 3, 'Dhaka', 'allow', '0000-00-00 00:00:00', 1),
(11, 4, ' Comilla', 'notallow', '0000-00-00 00:00:00', 0),
(12, 3, 'Dhaka', 'notallow', '0000-00-00 00:00:00', 0),
(13, 3, ' Chittagong', 'notallow', '0000-00-00 00:00:00', 0),
(14, 3, ' Rajshahi', 'notallow', '0000-00-00 00:00:00', 0),
(15, 3, 'Dhaka', 'notallow', '0000-00-00 00:00:00', 0),
(16, 3, ' Chittagong', 'notallow', '0000-00-00 00:00:00', 0),
(17, 3, ' Rajshahi', 'notallow', '0000-00-00 00:00:00', 0),
(18, 3, 'Dhaka', 'notallow', '0000-00-00 00:00:00', 0),
(19, 3, ' Chittagong', 'notallow', '0000-00-00 00:00:00', 0),
(20, 3, ' Rajshahi', 'notallow', '0000-00-00 00:00:00', 0),
(21, 3, 'Dhaka', 'notallow', '2013-06-20 07:08:02', 1),
(22, 3, ' Chittagong', 'notallow', '2013-06-20 07:08:02', 1),
(23, 3, ' Rajshahi', 'notallow', '2013-06-20 07:08:02', 1),
(24, 3, 'Dhaka', 'notallow', '2013-06-20 07:13:47', 1),
(25, 3, ' Barisal', 'notallow', '2013-06-20 07:13:47', 1),
(26, 3, ' Hghgh', 'notallow', '2013-06-20 07:19:35', 1),
(27, 3, ' Narayangonj', 'notallow', '2013-06-20 07:30:21', 1),
(28, 2, 'kkk', 'notallow', '2013-06-20 09:43:39', 1);

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE IF NOT EXISTS `colors` (
  `color_id` int(5) NOT NULL AUTO_INCREMENT,
  `color_title` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `color_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `color_image_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `color_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `color_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`color_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`color_id`, `color_title`, `color_code`, `color_image_name`, `color_updated`, `color_updated_by`) VALUES
(1, 'Test Color 1', '470047', 'img1.jpg', '2013-06-03 05:21:08', 1),
(2, 'Test Color 2', '7bff00', 'b6.jpg', '2013-06-03 05:35:11', 1),
(3, 'Product 1', 'ff00ff', 'b6.jpg', '2013-06-03 09:21:41', 1),
(4, 'AAaa 123 !!!!!!', 'ff00ff', 'img1.jpg', '2013-06-03 11:28:06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `config_settings`
--

CREATE TABLE IF NOT EXISTS `config_settings` (
  `CS_option` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `CS_value` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='config_setting =CS';

--
-- Dumping data for table `config_settings`
--

INSERT INTO `config_settings` (`CS_option`, `CS_value`) VALUES
('SITE_NAME', 'Ecommerce Framework'),
('SITE_URL', 'http://localhost/ecommerce'),
('SITE_LOGO', 'logo.jpg'),
('SITE_FAVICON', 'favicon.ico'),
('CATEGORY_BANNER_MAX_SIZE', '10000'),
('CATEGORY_BANNER_MAX_WIDTH', '500'),
('SITE_DEFAULT_META_TITLE', 'ecommerce'),
('SITE_DEFAULT_META_DESCRIPTION', 'this default description'),
('SITE_DEFAULT_META_KEYWORDS', 'cloth, bangladesh, dhaka, fashion'),
('PRODUCT_LARGE_IMAGE_WIDTH', '600'),
('PRODUCT_MEDIUM_IMAGE_WIDTH', '400'),
('PRODUCT_SMALL_IMAGE_WIDTH', '200'),
('GOOGLE_ANALYTICS', 'place google analytics code here');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `country_id` int(5) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `country_status` enum('allow','notallow') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'notallow',
  `country_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `country_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `country_name`, `country_status`, `country_updated`, `country_updated_by`) VALUES
(1, 'China', 'allow', '2013-06-19 12:03:11', 1),
(2, 'Australia', 'notallow', '2013-06-19 12:03:21', 1),
(3, 'Bangladesh', 'allow', '2013-06-19 12:03:29', 1),
(4, 'Russia', 'allow', '2013-06-20 04:52:46', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_user_id` int(11) NOT NULL,
  `order_created` datetime NOT NULL,
  `order_number` varchar(150) COLLATE utf8_unicode_ci NOT NULL COMMENT 'default null. entered by admin when processing orders',
  `order_read` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL COMMENT 'if order has been viewed',
  `order_status` enum('booking','approved','delivered','paid','closed') COLLATE utf8_unicode_ci NOT NULL COMMENT 'Only can change at booking stage',
  `order_payment_type` enum('COD','Paypal','Walleto') COLLATE utf8_unicode_ci NOT NULL COMMENT 'create table for every option',
  `order_total_item` int(3) NOT NULL,
  `order_total_amount` decimal(2,0) NOT NULL COMMENT 'summation of all product price',
  `order_vat_amount` decimal(2,0) NOT NULL,
  `order_discount_amount` decimal(2,0) NOT NULL COMMENT 'summation of all product discount',
  `order_session_id` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `order_note` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `order_billing_first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `order_billing_middle_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `order_billing_last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `order_billing_phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `order_billing_address` text COLLATE utf8_unicode_ci NOT NULL,
  `order_billing_country` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `order_billing_city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `order_billing_zip` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `order_shipping_first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `order_shipping_middle_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `order_shipping_last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `order_shipping_phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `order_shipping_address` text COLLATE utf8_unicode_ci NOT NULL,
  `order_shipping_country` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `order_shipping_city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `order_shipping_zip` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `order_updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE IF NOT EXISTS `order_products` (
  `OP_id` int(5) NOT NULL AUTO_INCREMENT,
  `OP_order_id` int(5) NOT NULL,
  `OP_user_id` int(11) NOT NULL,
  `OP_product_id` int(5) NOT NULL,
  `OP_product_inventory_id` int(11) NOT NULL,
  `OP_color_id` int(5) NOT NULL,
  `OP_size_id` int(5) NOT NULL,
  `OP_price` decimal(2,0) NOT NULL,
  `OP_discount` decimal(2,0) NOT NULL,
  `OP_product_quantity` int(5) NOT NULL,
  `OP_product_total_price` decimal(2,0) NOT NULL COMMENT 'total price = quantity * price',
  PRIMARY KEY (`OP_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='OP= order_products' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `paypal_payment`
--

CREATE TABLE IF NOT EXISTS `paypal_payment` (
  `PP_id` int(5) NOT NULL AUTO_INCREMENT,
  `PP_order_id` int(5) NOT NULL,
  `PP_user_id` int(11) NOT NULL,
  `PP_total_amount` decimal(2,0) NOT NULL,
  `PP_status` enum('unpaid','paid','closed') COLLATE utf8_unicode_ci NOT NULL COMMENT 'It will close when order closes',
  `PP_payment_gross` decimal(2,0) NOT NULL,
  `PP_payment_currency` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PP_varify_sign` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `PP_payer_email` int(150) NOT NULL,
  `PP_receiver_email` varchar(150) COLLATE utf8_unicode_ci NOT NULL COMMENT 'site owner or shop owner',
  `PP_txn_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `PP_ip_track_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `PP_full_data` text COLLATE utf8_unicode_ci NOT NULL,
  `PP_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`PP_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='PP= paypal_payment' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `product_sku` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `product_short_description` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `product_long_description` text COLLATE utf8_unicode_ci NOT NULL,
  `product_meta_title` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `product_meta_keywords` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `product_meta_description` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `product_quantity` int(4) NOT NULL,
  `product_weight` int(5) NOT NULL,
  `product_cost` decimal(10,0) NOT NULL,
  `product_price` decimal(10,0) NOT NULL,
  `product_discount_price` decimal(10,0) NOT NULL,
  `product_show_as_new_from` date NOT NULL,
  `product_show_as_new_to` date NOT NULL,
  `product_show_as_featured_from` date NOT NULL,
  `product_show_as_featured_to` date NOT NULL,
  `product_status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL,
  `product_total_viewed` int(11) NOT NULL,
  `product_tatal_sale` int(11) NOT NULL,
  `product_tax_class_id` int(11) NOT NULL,
  `product_avg_rating` int(2) NOT NULL,
  `product_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `product_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='weight will be gram' AUTO_INCREMENT=16 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_title`, `product_sku`, `product_short_description`, `product_long_description`, `product_meta_title`, `product_meta_keywords`, `product_meta_description`, `product_quantity`, `product_weight`, `product_cost`, `product_price`, `product_discount_price`, `product_show_as_new_from`, `product_show_as_new_to`, `product_show_as_featured_from`, `product_show_as_featured_to`, `product_status`, `product_total_viewed`, `product_tatal_sale`, `product_tax_class_id`, `product_avg_rating`, `product_updated`, `product_updated_by`) VALUES
(1, 'One ', '1122', 'my short description', 'my long description ', 'asdad111', 'asdads23123', 'asdasda1111', 0, 0, 1, 1234, 123, '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'active', 0, 0, 0, 4, '2013-05-14 04:14:59', 1),
(2, 'two', '125', 'short description', 'description dwo ', 'asdad111', 'asdads23123', 'asdasda1111', 0, 0, 1, 1234, 123, '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'active', 0, 0, 0, 0, '2013-05-14 04:17:45', 1),
(3, 'rwerwe', 'rwerwe', 'qweqwe', 'rewrew', 'asdad111', 'asdads23123', 'asdasda1111', 0, 0, 1, 1234, 123, '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'active', 0, 0, 0, 0, '2013-05-18 05:16:01', 1),
(4, 'ewr', 'wer', '', 'wer', 'asdad111', 'asdads23123', 'asdasda1111', 0, 0, 1, 1234, 123, '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'active', 0, 0, 0, 0, '2013-05-18 06:18:02', 1),
(5, 'asdasda', 'sdasdasd', '', 'asdasddasda', 'asdad111', 'asdads23123', 'asdasda1111', 0, 0, 1, 1234, 123, '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'active', 0, 0, 0, 0, '2013-05-20 10:19:31', 1),
(6, 'qwqw', 'qwqw', '', 'qwqw', 'asdad111', 'asdads23123', 'asdasda1111', 0, 0, 1, 1234, 123, '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'active', 0, 0, 0, 0, '2013-05-20 10:26:05', 1),
(7, 'qwqw1', 'wewe1', '', 'wewe2', 'asdad111', 'asdads23123', 'asdasda1111', 0, 0, 1, 1234, 123, '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'active', 0, 0, 0, 0, '2013-05-21 06:54:11', 1),
(8, 'dsdfsdf', 'asdad123!!', '', 'sdfsf', 'asdad111', 'asdads23123', 'asdasda1111', 0, 0, 1, 1234, 123, '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'active', 0, 0, 0, 0, '2013-05-21 08:31:16', 1),
(9, 'asdasda', 'ABCE123456', '', 'qweqwe', 'asdad111', 'asdads23123', 'asdasda1111', 0, 0, 1, 1234, 123, '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'active', 0, 0, 0, 0, '2013-05-21 08:50:38', 1),
(10, 'asda', 'ABCD123456', '', 'asdasd', 'asdad111', 'asdads23123', 'asdasda1111', 0, 0, 1, 1234, 123, '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'active', 0, 0, 0, 0, '2013-05-22 06:51:06', 1),
(11, 'Test Product 11', 'ABCD123456', 'Short description', 'Test Product 11', 'asdad111', 'asdads23123', 'asdasda1111', 0, 0, 1, 1234, 123, '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'active', 0, 0, 0, 0, '2013-05-30 08:09:48', 1),
(12, 'sasasfassfasfasfasfas', 'fasfasfasfasfas', '', 'asfasfasfasfasfaf', '', '', '', 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'active', 0, 0, 0, 0, '2013-06-11 07:25:35', 1),
(13, 'sadasdasd', 'asdasdasd', '', 'asdasdasdasd', '', '', '', 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'active', 0, 0, 0, 0, '2013-06-11 07:26:09', 1),
(14, 'asdasdasdasd', 'asd12', '', 'asdasdasdasdasdad', '', '', '', 0, 0, 0, 1, 0, '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'active', 0, 0, 0, 7, '2013-06-11 07:27:21', 1),
(15, 'sasdasdasdasdadasd', 'sasdasdasdasdadasd', '', 'sasdasdasdasdadasd', '', '', '', 0, 0, 0, 1, 0, '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 'active', 0, 0, 0, 5, '2013-06-11 07:29:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products_related`
--

CREATE TABLE IF NOT EXISTS `products_related` (
  `PR_id` int(11) NOT NULL AUTO_INCREMENT,
  `PR_current_product_id` int(11) NOT NULL,
  `PR_related_product_id` int(11) NOT NULL COMMENT '(not equal to current product ID, if current product ID =4 , related product ID cannot be 4)',
  `PR_priority_id` int(3) NOT NULL,
  `PR_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PR_created_by_id` int(11) NOT NULL,
  PRIMARY KEY (`PR_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='products_related = PR' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `products_related`
--

INSERT INTO `products_related` (`PR_id`, `PR_current_product_id`, `PR_related_product_id`, `PR_priority_id`, `PR_created`, `PR_created_by_id`) VALUES
(1, 1, 2, 0, '2013-05-25 09:13:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products_upsell`
--

CREATE TABLE IF NOT EXISTS `products_upsell` (
  `PU_id` int(11) NOT NULL AUTO_INCREMENT,
  `PU_current_product_id` int(11) NOT NULL,
  `PU_related_product_id` int(11) NOT NULL COMMENT '(not equal to current product ID, if current product ID =4 , related product ID cannot be 4)',
  `PU_priority_id` int(3) NOT NULL,
  `PU_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PU_created_by_id` int(11) NOT NULL,
  PRIMARY KEY (`PU_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='products_upsell = PU' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `products_upsell`
--

INSERT INTO `products_upsell` (`PU_id`, `PU_current_product_id`, `PU_related_product_id`, `PU_priority_id`, `PU_created`, `PU_created_by_id`) VALUES
(1, 1, 2, 0, '2013-05-25 11:01:19', 1),
(2, 1, 10, 5, '2013-05-25 11:01:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_also_like`
--

CREATE TABLE IF NOT EXISTS `product_also_like` (
  `PAL_id` int(11) NOT NULL AUTO_INCREMENT,
  `PAL_current_product_id` int(11) NOT NULL,
  `PAL_related_product_id` int(11) NOT NULL COMMENT '(not equal to current product ID, if current product ID =4 , related product ID cannot be 4)',
  `PAL_priority_id` int(3) NOT NULL,
  `PAL_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PAL_created_by_id` int(11) NOT NULL,
  PRIMARY KEY (`PAL_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='product_also_like = PAL' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `product_also_like`
--

INSERT INTO `product_also_like` (`PAL_id`, `PAL_current_product_id`, `PAL_related_product_id`, `PAL_priority_id`, `PAL_created`, `PAL_created_by_id`) VALUES
(5, 1, 5, 0, '2013-05-25 11:25:05', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE IF NOT EXISTS `product_categories` (
  `PC_id` int(11) NOT NULL AUTO_INCREMENT,
  `PC_product_id` int(11) NOT NULL,
  `PC_category_id` int(11) NOT NULL,
  `PC_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PC_created_by` int(11) NOT NULL,
  PRIMARY KEY (`PC_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='product_categories = PC' AUTO_INCREMENT=14 ;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`PC_id`, `PC_product_id`, `PC_category_id`, `PC_created`, `PC_created_by`) VALUES
(2, 2, 0, '2013-05-18 10:28:05', 0),
(7, 4, 1, '2013-05-26 07:05:18', 0),
(8, 11, 0, '2013-05-30 10:52:55', 0),
(13, 1, 0, '2013-06-02 05:22:22', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_discounts`
--

CREATE TABLE IF NOT EXISTS `product_discounts` (
  `PD_id` int(11) NOT NULL AUTO_INCREMENT,
  `PD_product_id` int(11) NOT NULL,
  `PD_start_date` date NOT NULL,
  `PD_end_date` date NOT NULL,
  `PD_amount` float NOT NULL,
  `PD_status` int(11) NOT NULL DEFAULT '1',
  `PD_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PD_updated_by` int(11) NOT NULL,
  PRIMARY KEY (`PD_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `product_discounts`
--

INSERT INTO `product_discounts` (`PD_id`, `PD_product_id`, `PD_start_date`, `PD_end_date`, `PD_amount`, `PD_status`, `PD_updated`, `PD_updated_by`) VALUES
(1, 5, '2013-06-01', '2013-06-05', 5, 0, '0000-00-00 00:00:00', 0),
(2, 5, '2013-06-08', '2013-06-10', 5, 0, '0000-00-00 00:00:00', 0),
(3, 5, '2013-06-11', '2013-06-15', 10, 0, '2013-06-11 10:26:54', 1),
(4, 4, '2013-06-01', '2013-06-10', 15, 0, '2013-06-15 08:38:44', 1),
(5, 0, '2013-06-01', '2013-06-10', 5, 1, '2013-06-15 09:20:09', 1),
(7, 1, '2013-06-25', '2013-06-26', 9, 0, '2013-06-15 09:45:56', 1),
(8, 4, '2013-06-01', '2013-06-10', 9, 1, '2013-06-16 04:35:38', 1),
(9, 4, '2013-06-11', '2013-06-12', 10, 0, '2013-06-16 04:47:09', 1),
(10, 4, '2013-06-11', '2013-06-12', 15, 0, '2013-06-16 04:48:33', 1),
(11, 4, '2013-06-11', '2013-06-12', 15, 0, '2013-06-16 04:50:01', 1),
(12, 4, '2013-06-15', '2013-06-16', 15, 0, '2013-06-16 04:50:09', 1),
(13, 1, '2013-06-01', '2013-06-04', 5, 0, '2013-06-18 08:14:40', 1),
(14, 1, '2013-06-05', '2013-06-10', 5, 0, '2013-06-18 08:14:54', 1),
(15, 1, '2013-06-05', '2013-06-10', 5, 1, '2013-06-18 08:15:25', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE IF NOT EXISTS `product_images` (
  `PI_id` int(11) NOT NULL AUTO_INCREMENT,
  `PI_product_id` int(11) NOT NULL,
  `PI_file_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `PI_priority` int(6) NOT NULL,
  `PI_color` int(11) NOT NULL,
  `PI_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PI_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`PI_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='PI = product_images' AUTO_INCREMENT=30 ;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`PI_id`, `PI_product_id`, `PI_file_name`, `PI_priority`, `PI_color`, `PI_updated`, `PI_updated_by`) VALUES
(17, 3, '3-1368869607.jpg', 0, 1, '2013-05-18 09:33:27', 1),
(20, 3, '3-1368869899.jpg', 0, 1, '2013-05-18 09:38:19', 1),
(21, 1, '1-1368871859.jpg', 0, 1, '2013-05-18 10:10:59', 1),
(22, 2, '2-1368872859.jpg', 0, 1, '2013-05-18 10:27:39', 1),
(23, 0, '-1369126927.ico', 0, 1, '2013-05-21 09:02:07', 1),
(24, 0, '-1369127171.jpg', 0, 1, '2013-05-21 09:06:11', 1),
(27, 5, '5-1369131076.jpg', 0, 1, '2013-05-21 10:11:16', 1),
(28, 5, '5-1369131151.jpg', 0, 1, '2013-05-21 10:12:31', 1),
(29, 4, '4-1370238586.jpg', 0, 1, '2013-06-03 05:49:46', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_inventories`
--

CREATE TABLE IF NOT EXISTS `product_inventories` (
  `PI_id` int(11) NOT NULL AUTO_INCREMENT,
  `PI_product_id` int(11) NOT NULL,
  `PI_color_id` int(11) NOT NULL,
  `PI_size_id` int(11) NOT NULL,
  `PI_quantity` int(5) NOT NULL,
  `PI_weight` int(5) NOT NULL,
  `PI_cost` decimal(10,0) NOT NULL,
  `PI_price` decimal(10,0) NOT NULL,
  `PI_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PI_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`PI_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='PI = product_inventories' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `product_inventories`
--

INSERT INTO `product_inventories` (`PI_id`, `PI_product_id`, `PI_color_id`, `PI_size_id`, `PI_quantity`, `PI_weight`, `PI_cost`, `PI_price`, `PI_updated`, `PI_updated_by`) VALUES
(1, 1, 1, 1, 10, 100, 1000, 10, '2013-06-02 05:25:09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_rating`
--

CREATE TABLE IF NOT EXISTS `product_rating` (
  `PR_id` int(5) NOT NULL AUTO_INCREMENT,
  `PR_product_id` int(5) NOT NULL,
  `PR_user_id` int(11) NOT NULL,
  `PR_rating_amount` int(5) NOT NULL,
  PRIMARY KEY (`PR_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_review`
--

CREATE TABLE IF NOT EXISTS `product_review` (
  `PRE_id` int(11) NOT NULL AUTO_INCREMENT,
  `PRE_product_id` int(11) NOT NULL,
  `PRE_user_id` int(5) NOT NULL,
  `PRE_comment` varchar(500) NOT NULL,
  `PRE_status` enum('draft','publish') NOT NULL,
  `PRE_read` enum('yes','no') NOT NULL,
  PRIMARY KEY (`PRE_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `product_review`
--

INSERT INTO `product_review` (`PRE_id`, `PRE_product_id`, `PRE_user_id`, `PRE_comment`, `PRE_status`, `PRE_read`) VALUES
(1, 3, 1, 'This is a sample review This is a sample review This is a sample review This is a sample review', 'draft', 'no'),
(2, 1, 1, 'This is a product review and you all can view it and read it easily.', 'draft', ''),
(3, 1, 1, 'This is a product review and you all can view it and read it easily.', 'draft', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `product_tags`
--

CREATE TABLE IF NOT EXISTS `product_tags` (
  `PT_id` int(11) NOT NULL AUTO_INCREMENT,
  `PT_product_id` int(11) NOT NULL,
  `PT_tag_id` int(11) NOT NULL,
  `PT_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PT_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`PT_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='PT = product_tags' AUTO_INCREMENT=10 ;

--
-- Dumping data for table `product_tags`
--

INSERT INTO `product_tags` (`PT_id`, `PT_product_id`, `PT_tag_id`, `PT_updated`, `PT_updated_by`) VALUES
(1, 1, 1, '2013-05-25 11:46:06', 1),
(2, 1, 3, '2013-05-25 11:46:06', 1),
(3, 1, 6, '2013-05-25 11:46:06', 1),
(4, 1, 7, '2013-05-25 11:46:37', 1),
(5, 1, 8, '2013-05-25 11:46:37', 1),
(6, 11, 2, '2013-05-30 11:17:39', 1),
(7, 11, 3, '2013-05-30 11:17:39', 1),
(8, 1, 3, '2013-06-02 05:27:04', 1),
(9, 1, 9, '2013-06-02 05:27:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_view`
--

CREATE TABLE IF NOT EXISTS `product_view` (
  `PV_id` int(15) NOT NULL AUTO_INCREMENT,
  `PV_product_id` int(5) NOT NULL,
  `PV_session_id` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `PV_visitor_ip` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PV_created` datetime NOT NULL,
  PRIMARY KEY (`PV_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='empty regularly using cron, PV= product_view table' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE IF NOT EXISTS `sizes` (
  `size_id` int(5) NOT NULL AUTO_INCREMENT,
  `size_title` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `size_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `size_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`size_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`size_id`, `size_title`, `size_updated`, `size_updated_by`) VALUES
(1, '30-45', '2013-04-29 08:36:17', 1),
(2, '40-41', '2013-04-29 08:37:26', 1),
(4, '20-21', '2013-04-29 08:45:39', 1),
(5, '20-25', '2013-04-30 06:52:00', 1),
(6, '40-41', '2013-06-03 08:17:59', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `tag_id` int(5) NOT NULL AUTO_INCREMENT,
  `tag_title` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tag_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tag_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`tag_id`, `tag_title`, `tag_updated`, `tag_updated_by`) VALUES
(1, 'asdad2232342342', '2013-04-28 05:07:43', 1),
(2, 'asdasdasd', '2013-04-29 05:43:03', 1),
(3, 'asdasdggrrgrrrgrg', '2013-04-29 05:49:05', 1),
(4, 'asda,asdasasd,asdad', '2013-04-29 06:10:47', 1),
(6, 'asdasd', '2013-04-29 06:32:53', 1),
(7, 'dasdasda', '2013-04-30 06:42:46', 1),
(8, 'asdasdasdasdasdasd', '2013-04-30 06:48:17', 1),
(9, 'asdasdad', '2013-05-20 10:18:57', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tax_classes`
--

CREATE TABLE IF NOT EXISTS `tax_classes` (
  `TC_id` int(5) NOT NULL AUTO_INCREMENT,
  `TC_title` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `TC_percent` decimal(10,0) NOT NULL,
  `TC_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `TC_updated_by` int(5) NOT NULL,
  PRIMARY KEY (`TC_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='TC = tax_classes' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tax_classes`
--

INSERT INTO `tax_classes` (`TC_id`, `TC_title`, `TC_percent`, `TC_updated`, `TC_updated_by`) VALUES
(1, 'asda', 19, '2013-04-30 08:31:43', 1),
(2, 'ewr', 10, '2013-04-30 08:37:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `temp_carts`
--

CREATE TABLE IF NOT EXISTS `temp_carts` (
  `TC_id` int(5) NOT NULL AUTO_INCREMENT,
  `TC_session_id` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TC_product_id` int(5) NOT NULL,
  `TC_product_inventory_id` int(5) NOT NULL,
  `TC_color_id` int(5) NOT NULL,
  `TC_size_id` int(5) NOT NULL,
  `TC_price` decimal(2,0) NOT NULL,
  `TC_discount_amount` decimal(2,0) NOT NULL,
  `TC_product_quantity` int(5) NOT NULL,
  `TC_product_total_price` decimal(2,0) NOT NULL COMMENT 'total price = quantity * price',
  `TC_created` datetime NOT NULL,
  PRIMARY KEY (`TC_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='empty regularly using cron, TC= temp_carts' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_hash` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `user_status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL,
  `user_first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_middle_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_DOB` date NOT NULL COMMENT 'date of birth',
  `user_gender` enum('male','female') COLLATE utf8_unicode_ci NOT NULL,
  `user_aboutme` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `user_profile_image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_address` text COLLATE utf8_unicode_ci NOT NULL,
  `user_phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_last_login` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_email`, `user_password`, `user_hash`, `user_status`, `user_first_name`, `user_middle_name`, `user_last_name`, `user_DOB`, `user_gender`, `user_aboutme`, `user_profile_image`, `user_address`, `user_phone`, `user_last_login`) VALUES
(1, 'mijo.jfwg@gmail.com', '123456', '', 'active', 'William', 'Francis', 'Gomes', '1986-07-10', 'male', 'I am William', '', 'Sample Address', '01914272921', '0000-00-00 00:00:00'),
(2, 'mijo.jfwg@gmail.com', '123456', '', 'active', 'William', 'Francis', 'Gomes', '1986-07-10', 'male', 'I am William', '', 'Sample Address', '01914272921', '1986-07-10 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE IF NOT EXISTS `user_addresses` (
  `UA_id` int(5) NOT NULL AUTO_INCREMENT,
  `UA_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `UA_first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `UA_middle_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `UA_last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `UA_phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `UA_best_call_time` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `UA_country_id` int(5) NOT NULL,
  `UA_city_id` int(5) NOT NULL,
  `UA_zip` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `UA_address` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `UA_updated` datetime NOT NULL,
  PRIMARY KEY (`UA_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='UA= user_addresses' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `walleto_payment`
--

CREATE TABLE IF NOT EXISTS `walleto_payment` (
  `WP_id` int(5) NOT NULL AUTO_INCREMENT,
  `WP_order_id` int(5) NOT NULL,
  `WP_user_id` int(11) NOT NULL,
  `WP_total_amount` decimal(2,0) NOT NULL,
  `WP_status` enum('unpaid','paid','closed') COLLATE utf8_unicode_ci NOT NULL COMMENT 'It will close when order closes',
  PRIMARY KEY (`WP_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='design based on walleto api, WP= walleto_payment' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE IF NOT EXISTS `wishlists` (
  `WL_id` int(5) NOT NULL AUTO_INCREMENT,
  `WL_product_id` int(5) NOT NULL,
  `WL_user_id` int(11) NOT NULL,
  `WL_cteated` datetime NOT NULL,
  PRIMARY KEY (`WL_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='WL= wishlists ' AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
