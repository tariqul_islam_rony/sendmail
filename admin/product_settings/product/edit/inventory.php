<?php
include ('../../../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}
$aid = @$_SESSION['admin_id']; //get admin id
//saving tags in database

/* if(isset($_POST['submit']))
  {
  $prosql = mysqli_query($con,"INSERT INTO products(product_title,product_sku,product_long_description,product_price) VALUES('$title','$sku','$desc','$price')");
  $curid = mysqli_insert_id($con);

  } */
$pid = base64_decode($_GET['pid']);


if (isset($_POST['update'])) {
    extract($_POST);
    $err = "";
    $msg = "";

    if ($color == "") {
        $err = "Product color is required.";
    } elseif ($size == "") {
        $err = "Product Size is required.";
    } elseif ($quan == "") {
        $err = "Product Quantity is required.";
    } elseif ($weight == "") {
        $err = "Product Weight is required.";
    } elseif ($cost == "") {
        $err = "Product Cost is required.";
    } elseif (!empty ($price)) {
         if (!is_numeric($price)) {
        $err = "Product Price can only be numeric.";
         }
    }elseif (!is_numeric($cost)) {
        $err = "Product Cost can only be numeric.";
    } else {

        $UpdateInventory = '';
        $UpdateInventory .= ' PI_product_id = "' . mysqli_real_escape_string($con, $pid) . '"';
        $UpdateInventory .= ', PI_color_id = "' . mysqli_real_escape_string($con, $color) . '"';
        $UpdateInventory .= ', PI_size_id = "' . mysqli_real_escape_string($con, $size) . '"';
        $UpdateInventory .= ', PI_quantity = "' . mysqli_real_escape_string($con, $quan) . '"';
        $UpdateInventory .= ', PI_weight = "' . mysqli_real_escape_string($con, $weight) . '"';
        $UpdateInventory .= ', PI_cost = "' . mysqli_real_escape_string($con, $cost) . '"';
        $UpdateInventory .= ', PI_price = "' . mysqli_real_escape_string($con, $price) . '"';
        $UpdateInventory .= ', PI_updated_by = "' . mysqli_real_escape_string($con, $aid) . '"';

        $UpdateInventorySQL = "INSERT INTO product_inventories SET $UpdateInventory";
        $ExecuteInventory = mysqli_query($con, $UpdateInventorySQL);

        if ($ExecuteInventory) {
            $msg = "Product Inventory Information added successfully.";
        } else {
            if (DEBUG) {
                echo "ExecuteGeneralInfo error" . mysqli_error($con);
            }
            $err = "Product Inventory Information could not added";
        }
    }
}

$pid = $_GET['pid'];
$product = mysqli_query($con, "SELECT * FROM products WHERE product_id='$pid'");
$rowproduct = mysqli_fetch_array($product);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="shortcut icon" href="<?php echo baseUrl('admin/images/favicon.ico') ?>" />
        <title>Admin Panel | Product Inventory</title>

        <link href="<?php echo baseUrl('admin/css/main.css'); ?>" rel="stylesheet" type="text/css" />
        <link href='http://fonts.googleapis.com/css?family=Cuprum' rel='stylesheet' type='text/css' />
        <script src="<?php echo baseUrl('admin/js/jquery-1.4.4.js'); ?>" type="text/javascript"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, File upload, editor -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/spinner/ui.spinner.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/jquery-ui.min.js'); ?>"></script>  
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/fileManager/elfinder.min.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, File upload -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/wysiwyg/jquery.wysiwyg.js'); ?>"></script>
        <!--Effect on wysiwyg editor -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/wysiwyg/wysiwyg.image.js'); ?>"></script>
        <!--Effect on wysiwyg editor -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/wysiwyg/wysiwyg.link.js'); ?>"></script>
        <!--Effect on wysiwyg editor -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/wysiwyg/wysiwyg.table.js'); ?>"></script>
        <!--Effect on wysiwyg editor -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/dataTables/jquery.dataTables.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/dataTables/colResizable.min.js'); ?>"></script>
        <!--Effect on left error menu, top message menu -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/forms/forms.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/forms/autogrowtextarea.js'); ?>"></script>
        <!--Effect on left error menu, top message menu, File upload -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/forms/autotab.js'); ?>"></script>
        <!--Effect on left error menu, top message menu -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/forms/jquery.validationEngine.js'); ?>"></script>
        <!--Effect on left error menu, top message menu-->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/colorPicker/colorpicker.js'); ?>"></script>
        <!--Effect on left error menu, top message menu -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/uploader/plupload.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, File upload -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/uploader/plupload.html5.js'); ?>"></script>
        <!--Effect on file upload-->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/uploader/plupload.html4.js'); ?>"></script>
        <!--No effect-->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/uploader/jquery.plupload.queue.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, File upload -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/ui/jquery.tipsy.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,  -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/jBreadCrumb.1.1.js'); ?>"></script>
        <!--Effect on left error menu, File upload -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/cal.min.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/jquery.collapsible.min.js'); ?>"></script>
        <!--Effect on left error menu, File upload -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/jquery.ToTop.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio, -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/jquery.listnav.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/jquery.sourcerer.js'); ?>"></script>
        <!--Effect on left error menu, top message menu,Drowpdowns and selects, Checkbox and radio -->
        <script type="text/javascript" src="<?php echo baseUrl('admin/js/custom.js'); ?>"></script>
        <script>
            function clr(str)
            {
                if (str == "")
                {
                    document.getElementById("txtHint").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("shwClr").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajaxcolor.php?c=" + str, true);
                xmlhttp.send();
            }
        </script>   
        <script type="text/javascript">
            function redirect()
            {
                if (confirm('Do you want to leave Product Editing Module?'))
                {
                    window.location = "../index.php";
                }
            }

        </script>        
        <!--Effect on left error menu, top message menu, body-->
        <!--delete tags-->
        <script type="text/javascript">
            /*function del(pin_id1)
             {
             if(confirm('Are you sure to delete this tag!!'))
             {
             window.location='index.php?del='+pin_id1;
             }
             }*/
        </script>
        <!--end delete tags-->



    </head>

    <body>


<?php include basePath('admin/top_navigation.php'); ?>

<?php include basePath('admin/module_link.php'); ?>


        <!-- Content wrapper -->
        <div class="wrapper">

            <!-- Left navigation -->
            <div class="leftNav">
<?php include('left_navigation.php'); ?>
            </div>

            <!-- Content Start -->
            <div class="content">
                <div class="title"><h5>Inventory Module</h5></div>

                <!-- Notification messages -->
<?php include basePath('admin/message.php'); ?>

                <!-- Charts -->
                <div class="widget first">
                    <div class="head">
                        <h5 class="iGraph">Inventory</h5></div>
                    <div class="body">
                        <div class="charts" style="width: 700px; height: auto;">
                            <form action="inventory.php?pid=<?php echo $_GET['pid']; ?>" method="post" class="mainForm">

                                <!-- Input text fields -->
                                <fieldset>
                                    <div class="widget first">
                                        <div class="head"><h5 class="iList">Product Inventory</h5></div>

                                        <div class="rowElem noborder"><label>Product Quantity:</label><div class="formRight">
                                                <input name="quan" type="text" maxlength="20" />
                                            </div><div class="fix"></div></div>

                                        <div class="rowElem noborder"><label>Product Weight:</label><div class="formRight">
                                                <input name="weight" type="text" maxlength="20" />
                                            </div><div class="fix"></div></div>

                                        <div class="rowElem">
                                            <label>Product Color :</label>
                                            <div class="formRight">
                                                <select name="color"  onchange="clr(this.value)">
                                                    <option value="0">Select Product Color</option>	
<?php
$sqlcolor = mysqli_query($con, "SELECT * FROM colors");
while ($rowcolor = mysqli_fetch_array($sqlcolor)) {
    ?>
                                                        <option value="<?php echo $rowcolor['color_id']; ?>"><?php echo $rowcolor['color_title']; ?></option>
    <?php
}
?>
                                                </select>&nbsp;&nbsp;&nbsp;&nbsp;<div id="shwClr" style="position:relative; left:170px; bottom:10px">Select a color.</div>&nbsp;&nbsp;&nbsp;&nbsp;
                                            </div>

                                        </div>

                                        <div class="rowElem">
                                            <label>Product Size :</label>
                                            <div class="formRight">
                                                <select name="size" >
                                                    <option value="opt1">Select Product Size</option>
<?php
$sqlsiz = mysqli_query($con, "SELECT * FROM sizes");
while ($rowsiz = mysqli_fetch_array($sqlsiz)) {
    ?>
                                                        <option value="<?php echo $rowsiz['size_id']; ?>"><?php echo $rowsiz['size_title']; ?></option>
    <?php
}
?>
                                                </select>
                                            </div>

                                        </div>

                                        <div class="rowElem noborder"><label>Product Cost:</label><div class="formRight">
                                                <input name="cost" type="text" maxlength="20"/>
                                            </div><div class="fix"></div></div>

                                        <div class="rowElem noborder"><label>Product Price:</label><div class="formRight">
                                                <input name="price" type="text" maxlength="20"/>
                                            </div><div class="fix"></div></div>



                                        <input type="submit" name="update" value="Update Inventory Details" class="greyishBtn submitForm" />
                                        <div class="fix"></div>

                                    </div>
                                </fieldset>

                            </form>		


                        </div>

<?php
$invsql = mysqli_query($con, "SELECT * FROM product_inventories WHERE PI_product_id='$pid'");
$count = mysqli_num_rows($invsql);
if ($count > 0) {
    ?>                        
                            <div class="table">
                                <div class="head">
                                    <h5 class="iFrames">Inventory List</h5></div>
                                <table cellpadding="0" cellspacing="0" border="0" class="display" id="example">
                                    <thead>
                                        <tr>
                                            <th>Product Color</th>
                                            <th>Product Size</th>
                                            <th>Product Quantity</th>
                                            <th>Product Weight</th>
                                            <th>Product Cost</th>
                                            <th>Product Price</th>
                                            <th>Product Updated By</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
    <?php
    while ($invrow = mysqli_fetch_array($invsql)) {
        ?>                        

                                            <tr class="gradeA">
                                                <td>
        <?php
        //getting color from db 
        $c = $invrow['PI_color_id'];
        $csql = mysqli_query($con, "SELECT * FROM colors WHERE color_id='$c'");
        $crow = mysqli_fetch_array($csql);

        echo $crow['color_title'];
        ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    //getting product size from db
                                                    $s = $invrow['PI_size_id'];
                                                    $ssql = mysqli_query($con, "SELECT * FROM sizes WHERE size_id='$c'");
                                                    $srow = mysqli_fetch_array($ssql);

                                                    echo $srow['size_title'];
                                                    ?>
                                                </td>
                                                <td><?php echo $invrow['PI_quantity']; ?></td>
                                                <td><?php echo $invrow['PI_weight']; ?></td>
                                                <td><?php echo $invrow['PI_cost']; ?></td>
                                                <td><?php echo $invrow['PI_price']; ?></td>
                                                <td><?php
                                                    //getting admin name from database using id
                                                    $aid = $invrow['PI_updated_by'];
                                                    $adminsql = mysqli_query($con, "SELECT (admin_full_name) FROM admins WHERE admin_id='$aid'");
                                                    $adminrow = mysqli_fetch_array($adminsql);
                                                    echo $adminrow[0];
                                                    ?></td>
                                                <td class="center"><!--<a href="edit.php?id=<?php echo $invrow['PI_id']; ?>" title="Edit"><img src="../../images/pencil-grey-icon.png" height="14" width="14" alt="Edit" /></a>-->&nbsp;&nbsp;&nbsp;&nbsp;<!--<a href="javascript:del(<?php echo $invrow['PI_id']; ?>);"><img src="../images/delete.png" /></a>--></td>
                                            </tr>
        <?php
    }
}
?>
                                </tbody>
                            </table>
                        </div>

                    </div>









                </div>
            </div>

        </div>
        <!-- Content End -->

        <div class="fix"></div>
        </div>

<?php include basePath('admin/footer.php'); ?>
        <script type="text/javascript">
            var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
            var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
            var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea1");
        </script>
