        <!-- Header -->
        <div id="header" class="wrapper">
            <div class="logo"><a href="<?php echo baseUrl('admin/dashboard.php');?>" title=""><img src="<?php echo baseUrl('admin/images/loginLogo.png');?>" alt="" /></a></div>
            <div class="middleNav">
                <ul>
                    <li class="iUser"><a href="<?php echo baseUrl('admin/admin/');?>" title=""><span>Admin Module</span></a></li>
                    <li class="iStat"><a href="<?php echo baseUrl('admin/product_settings/package');?>" title=""><span>Package Module</span></a></li>
                    <li class="iStat"><a href="<?php echo baseUrl('admin/country/');?>" title=""><span>Country Module</span></a></li>
                    <li class="iStat"><a href="<?php echo baseUrl('admin/customer_activity/');?>" title=""><span>Customer Activity</span></a><span class="numberMiddle">9</span></li>
                </ul>
            </div>
            <div class="fix"></div>
        </div>